const Header = props => (
    <header className="home-header">
    <nav className="grey darken-3" role="navigation" >
        <div className="nav-wrapper">
            <a id="logo-container" className="brand-logo">
                <h5 className="light logo-text">EPAM STUDENT REGISTRATION ADMIN</h5>
            </a>
            <ul className="right hide-on-med-and-down">
                <li><a onClick={props.sideLinkOnClick}>{props.sideLinkLabel}</a></li>
            </ul>
            <a href="#" data-target="slide-out" className="sidenav-trigger"><i className="material-icons">menu</i></a>
        </div>
    </nav>
    </header>
);

export default Header;