import React, { Component } from 'react';
import Header from "../components/Header";
import Footer from "../components/Footer";

import AdminHome from './AdminHome';

class App extends Component {
  
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
  }

  logout() {
    console.log("Logging out...!")
  }

  render() {
    return (

      <section>
          <Header sideLinkLabel="Logout" sideLinkOnClick={this.logout}/>
          <div id="slide-out" className="sidenav sidenav-fixed grey darken-3 home-side-nav">
              <div className="valign-wrapper center">
                  <img className="epam-logo" src={require("../../images/EPAM_LOGO.png")} alt="epam-logo" />
              </div>
              <h5>
                  <a  className="light grey-text text-lighten-5">Admin Home</a>
              </h5>
              <h5>
                  <a className="light grey-text text-lighten-5" 
                      onClick={this.logout}>Logout <i className="material-icons teal-text">exit_to_app</i></a>
              </h5>

          </div>
          <main className="home-main">
          <div className="home-detail">
            <AdminHome/>
          </div>
          </main>

          <Footer />




      </section>
      
    )
  }
}



export default App;

//Future Use
//Validations
    // if(startDate.length===0){alert("Please Select Start date");return;}
    // if(endDate.length===0){alert("Please Select End  date");return;}
    // if(+new Date(startDate).getDate() > +new Date(endDate).getDate()){alert("Improper start date and end date selected");return;}
  
    
    // this.props.setStartDate(startDate);
    // this.props.setEndDate(endDate);


