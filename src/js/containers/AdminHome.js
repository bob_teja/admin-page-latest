import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";

// import '../css/App.css'

import { getStudentDetailsClicked, selectAll, deSelectAll} from "../actions/studentDetailsActions"
import {setStartDate, setEndDate, setStatus, setPassoutYear, setCollegeName, setPercentage} from "../actions/searchActions"
import StudentListItem from './StudentListItem';
import {rejectStudentAPICall,approveStudentAPICall} from '../actions/studentDetailsActions'
import {incrementIndex, decrementIndex, setNumberOfItems, setToOneIndex} from "../actions/paginationActions"

class AdminHome extends Component {
  
  constructor(props) {
    super(props);

    this.getData = this.getData.bind(this);
    this.selectAllChanged = this.selectAllChanged.bind(this);
  }



  getData  (event)  {
    event.preventDefault();
    // var startDate = document.getElementById("startDate").value;
    // var endDate = document.getElementById("endDate").value;
    // var status = document.getElementById("status").value;

    
    
    this.props.getStudentDetailsClicked();
  }

  selectAllChanged(event) {
      let isChecked = event.target.checked;
      if(isChecked) {
        this.props.selectAll();
      } else {
        this.props.deSelectAll();
      }
  }
  render() {
    const {studentDetails} = this.props.studentDetailsReducer;
    return (
      <div className="container">
          <div className="section">
      
              <div className="row">
                  <form onSubmit={this.getData}>
      
      
                      {/* <div className="input-field col s3">
                          <input type="text" id="startDate" className="datepicker" />
                          <label htmlFor="startDate">Start Date</label>
                      </div>
      
                      <div className="input-field col s3">
                          <input type="text" id="endDate" className="datepicker" />
                          <label htmlFor="endDate">End Date</label>
                      </div> */}

                      <div className="input-field col s2">
                          <input type="text" id="clgName" onChange={(event)=>{this.props.setCollegeName(event.target.value);}} />
                          <label htmlFor="clgName">College Name</label>
                      </div>
                      
                      <div className="input-field col s2">
                          <input type="number" id="percentage" onChange={(event)=>{this.props.setPercentage(event.target.value);}}/>
                          <label htmlFor="percentage">Percentage</label>
                      </div>

                      <div className="input-field col s2">
                          <select id="passoutYear" onChange={(event)=>{this.props.setPassoutYear(event.target.value);}}>
                              <option value="2017">2017</option>
                              <option value="2018">2018</option>
                              <option value="2019">2019</option>
                          </select>
                      </div>
      
                      <div className="input-field col s2">
                          <select id="status" onChange={(event)=>{this.props.setStatus(event.target.value);}}>
                              <option value="HOLD">Hold</option>
                              <option value="APPROVED">Approved</option>
                              <option value="REJECTED">Rejected</option>
                          </select>
                      </div>

                      <div className="input-field col s2">
                            <select id="numberOfItems" onChange={(event)=>{this.props.setNumberOfItems(event.target.value);}}>
                                <option value="" disabled selected>No. of Records</option>
                                <option value="1">1</option>
                                <option value="10">10</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                      </div>
      
                      <div className="input-field col s2">
                          <button className="btn waves-effect waves-light" type="submit" name="action">Get Details
                              <i className="material-icons right">send</i>
                          </button>
                      </div>

                      {/* <div className="input-field col s3">
                        <button className="btn waves-effect waves-light" type="button" name="select-all" onClick={this.props.selectAll}>Select all
                            <i className="material-icons right">send</i>
                        </button>
                      </div>
                      
                      <div className="input-field col s3">
                          <button className="btn waves-effect waves-light" type="button" name="deselct-all" onClick={this.props.deSelectAll}>Deselect all
                              <i className="material-icons right">send</i>
                          </button>
                      </div> */}
                  </form>
              </div>
      
              <div className="row center">
                  <div className="col s12 m8 offset-m2">
                      <h4 className="header light">Student Details</h4>
                  </div>
              </div>
              {
                (studentDetails !== null)
                    ? (studentDetails != "" )
                        ?
                            <section>
                                <div className="row card z-depth-3 valign-wrapper" style={{margin: "10px 0", padding: "10px"}}>
                                    <div className="col">           
                                        <label>
                                            <input type="checkbox" onChange={this.selectAllChanged}/>
                                            <span>Select all</span>
                                        </label>
                                    </div> 

                                    <div className="input-field col right-align valign-wrapper">
                                        <a className="right btn-floating btn-small waves-effect waves-light green" onClick={this.props.approveStudentAPICall}>
                                            <i className="material-icons">check</i>
                                        </a>
                                        <span>&nbsp; &nbsp; Accept Selected &nbsp; &nbsp;</span>
                                        {/* <button className="btn waves-effect waves-light " type="button" name="accept-selected" onClick={this.props.approveStudentAPICall}>Accept Selected
                                                <i className="material-icons right">send</i>
                                        </button> */}
                                    </div>
                                    <div className="input-field col right-align valign-wrapper">
                                        <a className="right btn-floating btn-small waves-effect waves-light red" onClick={this.props.rejectStudentAPICall}>
                                            <i className="material-icons">close</i>
                                        </a>
                                        <span>&nbsp; &nbsp; Reject Selected &nbsp; &nbsp;</span>
                                        {/* <button className="btn waves-effect waves-light " type="button" name="reject-selected" onClick={this.props.rejectStudentAPICall}>Reject Selected
                                            <i className="material-icons right">send</i>
                                        </button> */}
                                    </div>   

                                </div>
                                {   
                                    studentDetails.map((eachStudent)=>
                                    <StudentListItem key={Math.floor(Math.random()*1000000)} student={eachStudent} />)
                                }
                            </section>
                        :<h5 className="header light center">No Data</h5>
                    : <h5 className="header light center">Submit the form to load data</h5>
              }

            <div className="row center">
              <div className="col s12">
                <ul className="pagination">
                    <li className="waves-effect">
                        <a onClick={()=>{this.props.decrementIndex();this.props.getStudentDetailsClicked()}}>
                            <i className="material-icons">chevron_left</i>
                        </a>
                    </li>
                    <li className="waves-effect">
                        <a>{this.props.paginationReducer.index}</a>
                    </li>
                    <li className="waves-effect">
                        <a onClick={()=>{this.props.incrementIndex();this.props.getStudentDetailsClicked()}}>
                            <i className="material-icons">chevron_right</i>
                        </a>
                    </li>
                </ul>
              </div>
            </div> 

             
          </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    studentDetailsReducer: state.studentDetails,
    paginationReducer : state.pagination
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
      setStartDate : setStartDate,
      setEndDate : setEndDate,
      setStatus : setStatus,
      setPercentage : setPercentage,
      setPassoutYear : setPassoutYear,
      setCollegeName : setCollegeName,
      getStudentDetailsClicked : getStudentDetailsClicked,
      selectAll : selectAll,
      deSelectAll : deSelectAll,
      rejectStudentAPICall : rejectStudentAPICall,
      approveStudentAPICall : approveStudentAPICall,
      incrementIndex: incrementIndex,
      decrementIndex: decrementIndex,
      setToOneIndex:setToOneIndex,
      setNumberOfItems:setNumberOfItems

  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminHome);

//Future Use
//Validations
    // if(startDate.length===0){alert("Please Select Start date");return;}
    // if(endDate.length===0){alert("Please Select End  date");return;}
    // if(+new Date(startDate).getDate() > +new Date(endDate).getDate()){alert("Improper start date and end date selected");return;}
  
    
    // this.props.setStartDate(startDate);
    // this.props.setEndDate(endDate);


