import React, { Component } from 'react';
import {connect} from 'react-redux';
import {rejectStudentAPICall,approveStudentAPICall,setCheckedStatus} from '../actions/studentDetailsActions'
import {bindActionCreators} from "redux";


import CONST from "../utils/constants" 


class StudentListItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            imgLink : null
        }
        this.viewPhoto = this.viewPhoto.bind(this);
        this.handleChangeCheck = this.handleChangeCheck.bind(this);
    }

    handleChangeCheck(event) {
        const checkedStatus = event.target.checked;
        const id = this.props.student.id;
        this.props.setCheckedStatus(id, checkedStatus)
    }


    viewPhoto (event)  {
        event.preventDefault();
        const imgLink = CONST.URL+"user/secure/file?id="+this.props.student.id+"&type=image";
        console.log(imgLink);
        var image = new Image();
        image.src = imgLink;
        var w = window.open("");

        //Opens image in new tab
        w.document.write(image.outerHTML);
    }

    render() {
        const {student} = this.props;
        return (
            <div className="row card z-depth-3 valign-wrapper" style={{margin: "10px 0", padding: "10px"}}>

                <div className="col s1 m1">           
                    <label>
                        <input type="checkbox" defaultChecked={student.isChecked} onChange={this.handleChangeCheck}/>
                        <span></span>
                    </label>
                </div>    

                <div className="col s12 m2">
                    <span>{student.firstName}</span>
                </div>

                <div className="col s12 m2">
                    <span>{student.lastName}</span>
                </div>

                <div className="col s12 m2">
                    <span>{student.dateOfBirth}</span>
                </div>
                <div className="col s12 m3">
                    <span>{student.name}</span>
                </div>
                <div className="col s12 m2">
                    <a className="right btn-small waves-effect waves-light btn" onClick={this.viewPhoto}>View Govt.ID</a>
                </div>

                {/* <div className="col s12 m1">
                    <a className="right btn-floating btn-small waves-effect waves-light green" onClick={this.approveStudent}>
                        <i className="material-icons">check</i>
                    </a>
                </div>

                <div className="col s12 m1">
                    <a className="right btn-floating btn-small waves-effect waves-light red" onClick={this.rejectStudent}>
                        <i className="material-icons">close</i>
                    </a>
                </div> */}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
      studentDetailsReducer: state.studentDetails
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setCheckedStatus : setCheckedStatus
    }, dispatch);
  }
  
export default connect(mapStateToProps,mapDispatchToProps)(StudentListItem);