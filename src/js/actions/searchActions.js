import SEARCH_ACTION from "./searchActionsTypes"

export const setStartDate = (startDate) => {
    return {
        type: SEARCH_ACTION.SET_START_DATE,
        payload: startDate
    }
}

export const setEndDate = (endDate) => {
    return {
        type: SEARCH_ACTION.SET_END_DATE,
        payload: endDate
    }
}

export const setStatus = (status) => {
    return {
        type: SEARCH_ACTION.SET_STATUS,
        payload: status
    }
}

export const setPassoutYear = (year) => {
    return {
        type: SEARCH_ACTION.SET_PASSOUT_YEAR,
        payload: year
    }
}

export const setCollegeName = (collegeName) => {
    return {
        type: SEARCH_ACTION.SET_COLLEGE_NAME,
        payload: collegeName
    }
}

export const setPercentage = (percentage) => {
    return {
        type: SEARCH_ACTION.SET_PERCENTAGE,
        payload: percentage
    }
}