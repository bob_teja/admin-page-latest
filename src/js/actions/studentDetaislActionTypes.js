const STUDENT_DETAILS_ACTION = {
    ADD_STUDENT_DETAILS : "ADD_STUDENT_DETAILS",
    DELETE_STUDENT_DETAILS : "DELETE_STUDENT_DETAILS",
    CHANGE_CHECKED_STATUS : "CHANGE_CHECKED_STATUS"
    // APPROVE_STUDENT : "APPROVE_STUDENT",
    // REJECT_STUDENT : "REJECT_STUDENT"
}

export default STUDENT_DETAILS_ACTION;
