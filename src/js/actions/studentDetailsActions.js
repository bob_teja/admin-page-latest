import STUDENT_DETAILS_ACTION from "./studentDetaislActionTypes"
import PAGINATION_ACTION from "./paginationActionTypes"
import axios from 'axios';
import CONST from "../utils/constants"

export const setStudentDetails = (studentDetails) => {
    console.log(studentDetails)
    return {
        type: STUDENT_DETAILS_ACTION.ADD_STUDENT_DETAILS,
        payload: studentDetails
    }
};

export const getStudentDetailsClicked = () => (dispatch, getState) => {
    const {
        pagination,
        search
    } = getState();
    console.log("Clicked ")
    //let studentDetailsFetchAPI = CONST.URL + "admin/secure/studentInfo?name=NONE&status=" + status;
    let studentDetailsFetchAPI =
        CONST.URL + "admin/secure/studentInfo?name=" + search.collegeName +
        "&status=" + search.status +
        "&year=" + search.passoutYear +
        "&percentage=" + search.percentage +
        "&page=" + (pagination.index - 1) +
        "&size=" + pagination.numberOfItems + "";

    console.log(studentDetailsFetchAPI)
    axios.get(studentDetailsFetchAPI)
        .then(responseData => responseData.data)
        .then(data => {
            if (data == null) {
                return;
            }
            for (let i = 0; i < data.length; i++) {
                data[i].isChecked = false;
            }
            dispatch({
                type: STUDENT_DETAILS_ACTION.ADD_STUDENT_DETAILS,
                payload: data,
            });
        });
}

export const deleteStudentDetails = (id) => {
    return {
        type: STUDENT_DETAILS_ACTION.DELETE_STUDENT_DETAILS,
        payload: id
    }
}

export const approveStudentAPICall = (id) => (dispatch, getState) => {
    var {
        studentDetails
    } = getState();
    var details = studentDetails.studentDetails;
    const checkedData = details.filter((student) => student.isChecked).map((student) => student.id);
    // console.log(checkedData)
    var data = {
        "id": checkedData
    }
    let approveStudentFetchAPI = CONST.URL + "admin/secure/approveStudent";
    // console.log(approveStudentFetchAPI)
    axios.post(approveStudentFetchAPI, data)
        .then(responseData => responseData.data)
        .then(data => {
            if (data == null) {
                return;
            }
            if (data) {
                for (let each of checkedData) {
                    dispatch({
                        type: STUDENT_DETAILS_ACTION.DELETE_STUDENT_DETAILS,
                        payload: each,
                    });
                }
                dispatch({
                    type: PAGINATION_ACTION.SET_TO_ONE_INDEX,
                    payload: 1,
                });
                getStudentDetailsClicked()(dispatch, getState);
            }
        });
}

export const rejectStudentAPICall = (id) => (dispatch, getState) => {
    var {
        studentDetails
    } = getState();
    var details = studentDetails.studentDetails;
    const checkedData = details.filter((student) => student.isChecked).map((student) => student.id);
    var data = {
        "id": checkedData
    }

    let rejectStudentFetchAPI = CONST.URL + "admin/secure/rejectStudent";
    // console.log(rejectStudentFetchAPI)
    axios.post(rejectStudentFetchAPI, data)
        .then(responseData => responseData.data)
        .then(data => {
            if (data == null) {
                return;
            }
            if (data) {
                for (let each of checkedData) {
                    dispatch({
                        type: STUDENT_DETAILS_ACTION.DELETE_STUDENT_DETAILS,
                        payload: each,
                    });
                }
                dispatch({
                    type: PAGINATION_ACTION.SET_TO_ONE_INDEX,
                    payload: 1,
                });
                getStudentDetailsClicked()(dispatch, getState);
            }
        });
}

export const setCheckedStatus = (id, checkedStatus) => (dispatch, getState) => {
    var {
        studentDetails
    } = getState();
    var details = studentDetails.studentDetails;
    for (let i = 0; i < details.length; i++) {
        if (details[i].id === id) {
            details[i].isChecked = checkedStatus;
        }
    }
    return {
        type: STUDENT_DETAILS_ACTION.CHANGE_CHECKED_STATUS,
        payload: details
    }

}

export const selectAll = () => (dispatch, getState) => {
    //console.log("Select All");
    var {
        studentDetails
    } = getState();
    var details = studentDetails.studentDetails;
    if (details != null) {
        for (let i = 0; i < details.length; i++) {
            details[i].isChecked = true;
        }
        dispatch({
            type: STUDENT_DETAILS_ACTION.CHANGE_CHECKED_STATUS,
            payload: details,
        });
    }

}

export const deSelectAll = () => (dispatch, getState) => {
    var {
        studentDetails
    } = getState();
    var details = studentDetails.studentDetails;
    for (let i = 0; i < details.length; i++) {
        details[i].isChecked = false;
    }
    dispatch({
        type: STUDENT_DETAILS_ACTION.CHANGE_CHECKED_STATUS,
        payload: details,
    });

}
